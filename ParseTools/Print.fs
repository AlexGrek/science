﻿module Print

open Tools
open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open Microsoft.CodeAnalysis.MSBuild
open System.Text
open ParseUtils

let treeView (funct: SyntaxNode -> string) (node: SyntaxNode) =
    let rec tree indent (node: SyntaxNode) =
        printfn "%sL %s" (String.replicate indent "|   ") (funct node)
        Seq.iter (tree (indent + 1)) <| node.ChildNodes()

    tree 0 node


let treeStr (funct: SyntaxNode -> string) (node: SyntaxNode) =
    let stringb = StringBuilder()
    let rec tree indent (node: SyntaxNode) =
        let newstr = sprintf "%sL %s" (String.replicate indent "|   ") (funct node)
        stringb.AppendLine(newstr.PadRight(90) + "|") |> ignore
        Seq.iter (tree (indent + 1)) <| node.ChildNodes()
    tree 0 node
    stringb.ToString()


let printNames: SyntaxNode -> unit = treeView (fun x -> x.Kind().ToString())


let printNamesTypes: SyntaxNode -> unit = treeView (fun x -> 
                        x.Kind().ToString() + ": " + x.GetType().ToString())

let detailsOf (node: SyntaxNode) = 
        let rawDetails =
            match node with
            | :? IfStatementSyntax as s -> "If " + s.Condition.ToString()
            | :? BlockSyntax as s -> "BLOCK: " + s.Statements.ToString().Split('\n').Length.ToString()
            | :? ForStatementSyntax as s -> "For " + s.Condition.ToString()
            | :? WhileStatementSyntax as s -> "While " + s.Condition.ToString()
            | :? MethodDeclarationSyntax as s -> s.Identifier.ToString()
            | _ -> if node.ToString().Split('\n').Length = 1 then node.ToString() else node.Kind().ToString() + "..."
        rawDetails.Replace("\"", "''")


let detailsToString (node: SyntaxNode) =
    //if node = null then "NULL" else
    //    let name = node.Kind().ToString() + ":" +
    //        (ParseUtils.lineOf node).ToString() + ": "
    //    name + detailsOf node
    if node = null then "NULL" else
        let kind = node.Kind().ToString()
        let line = lineOf node
        let content = node.ToString().Replace("\"", "''").Replace("\n", String.Empty).Replace("\r", String.Empty)
        let cut = if content.Length > 10 then content.Substring(0, 7) + "..." 
                  else content
        sprintf "%s:%d::%s" kind line cut


let justDetailsToString (node: SyntaxNode) =
    let name =
        (ParseUtils.lineOf node).ToString() + ": "
    name + detailsOf node


let printDetails: SyntaxNode -> unit = 
    treeView detailsToString


let singleView (node: SyntaxNode) =
    printfn "%s" <| detailsToString node

