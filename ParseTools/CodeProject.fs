﻿module CodeProject

open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open Microsoft.CodeAnalysis.MSBuild
open System.IO
open System.Collections.Generic

let findProjectFile path =
    if File.Exists(path) then
        path
    else
        if Directory.Exists(path) then
            let files = Directory.EnumerateFiles(path, "*.csproj", SearchOption.TopDirectoryOnly) |> Seq.toList
            match files with
            | [ x ] -> x
            | [] -> failwith <| sprintf "File *.csproj not found in: %s" path
            | _ -> failwith <| sprintf "Multiple files *.csproj found in: %s" path
        else
            failwith <| sprintf "File not found: %s" path


let loadProjectFromPath path =
    let file = findProjectFile path
    let msWorkspace = MSBuildWorkspace.Create();
    let projAsync = Async.AwaitTask <| msWorkspace.OpenProjectAsync(file)
    Async.RunSynchronously projAsync, msWorkspace


let loadProjectFromSolution solution projectName =
    let paramsd = Dictionary<string, string>()
    paramsd.Add("CheckForSystemRuntimeDependency", "true")
    let msw = MSBuildWorkspace.Create(paramsd)
    msw.WorkspaceFailed.Add(fun x -> printfn "MSWORKSPACE error: %A" x)
    let solutionAsync = Async.AwaitTask <| msw.OpenSolutionAsync(solution)
    let sol = Async.RunSynchronously solutionAsync
    let target = Seq.find (fun (x:Project) -> x.Name = projectName) sol.Projects
    printfn "Project %s of type %A loaded" target.Name <| target.GetType()
    target, msw


let loadCompilation (project: Project) =
    let compilationAsync = Async.AwaitTask <| project.GetCompilationAsync()
    let comp = Async.RunSynchronously compilationAsync
    Seq.iter (fun (a: MetadataReference) -> printfn "ref: %A" a.Display) comp.References
    comp

type CodeFile(name: string, root: SyntaxTree, semantic: SemanticModel) =
    member this.Semantic = semantic
    member this.Name = name
    member this.GetRoot() =
        root.GetRoot()

    member this.Root = 
        root.GetRoot()

    member this.LOC() =
        root.GetText().Lines.Count
    
    member this.GetTypeOfExpression (syn: SyntaxNode) =
        Semantics.getTypeOfExpression this.Semantic syn


type CodeProject(proj: Project, compilation: Compilation, w: MSBuildWorkspace) =
    let project = proj
    let compilation = compilation

    member this.makeCodeFile (d: Document) =
        let projAsync = Async.AwaitTask <| d.GetSyntaxTreeAsync()
        let syntree = Async.RunSynchronously projAsync
        let semantic = compilation.GetSemanticModel(syntree)
        if ParseUtils.COMPILE_ERRORS then
            printfn "Workspace diagnostics: -------------------------------------"
            Seq.iter (fun (x: WorkspaceDiagnostic) -> printfn "DIAG: %A" <| x.ToString() )  <| w.Diagnostics
            printfn "Compilation diagnostics: -------------------------------------"
            Seq.iter (fun (x: Diagnostic) -> if x.Severity = DiagnosticSeverity.Error then printfn "DIAG: %A" <| x.ToString() )  <| compilation.GetDiagnostics()
            printfn "Semantic diagnostics: -------------------------------------"
            Seq.iter (fun (x: Diagnostic) -> if x.Severity = DiagnosticSeverity.Error then printfn "DIAG: %A" <| x.ToString() )  <| semantic.GetDiagnostics()
        CodeFile(d.Name, syntree, semantic)

    member this.GetFileByName name =
        Seq.tryFind (fun (el: Document) -> el.Name = name) project.Documents
        |> Option.map this.makeCodeFile

    member this.MustGetFileByName name =
        Seq.find (fun (el: Document) -> el.Name = name) project.Documents
        |> this.makeCodeFile

    member this.GetAllDocuments() =
        Seq.map this.makeCodeFile project.Documents

    member this.GetAllDocumentNames() =
        Seq.filter (fun (x: Document) -> x.SourceCodeKind = SourceCodeKind.Regular
                                            && x.SupportsSemanticModel 
                                            && x.SupportsSyntaxTree )
                                            project.Documents
        |> Seq.filter (fun (x: Document) -> (not <| x.Name.Contains(",")) && (not <| x.Name.Contains("=")))
        |> Seq.map (fun (x: Document) -> x.Name)

let loadProject path =
    let proj, w = loadProjectFromPath path
    let comp = loadCompilation proj
    CodeProject(proj, comp, w)

let loadProjectSolution path name =
    let proj, w = loadProjectFromSolution path name
    let comp = loadCompilation proj
    CodeProject(proj, comp, w)