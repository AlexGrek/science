﻿module Encode

open Convert

let encAtom = function
    | Expression -> 'x'
    | LocalVar -> 'v'
    | ClassField -> 'f'
    | MethodCall -> 'm'
    | Argument -> 'a'
    | Literal -> 'l'
    | ExternalField -> 'e'
    | ObjConstruction -> 'c'
    | Undefined -> '_'

let encType = function
    | String -> 'S'
    | Numeric -> 'N'
    | ClassType -> 'C'
    | Array -> 'A'
    | UndefinedType -> '*'

let encCallLocation = function
    | ThisClass -> 'c'
    | SystemCall -> 's'
    | ThisNamespace -> 'n'
    | ExternalCall -> 'e'

let encLoopKind = function
    | ForLoop -> '['
    | ForeachLoop -> '{'
    | WhileLoop -> '('

let encAssKind = function
    | Declaration -> 'd'
    | Local -> 'l'
    | Field -> 'f'
    | External -> 'e'

let encSomeData = function
    | Some(s: Atom) -> s.DataType |> encType |> string
    | None -> "#"
        

let encAssignmentSource = function
    | FromCall(call) ->
        let datatype = encSomeData <| call.ValArg
        "c" + (encCallLocation call.Location |> string) + datatype
    | FromAtom(a) -> 
        "a" + (encAtom a.Kind |> string) + (encType a.DataType |> string)
    | Empty -> "___"
    