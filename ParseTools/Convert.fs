﻿module Convert

open Microsoft.CodeAnalysis.CSharp.Syntax
open Microsoft.CodeAnalysis
open System

type Context = {Method: string; Namespace: string; ProjectPath: string}

type Method = {MethodName: string; MethodNamespace: string; MethodSymbol: ISymbol; File: string; Line: int}
        with
            override x.ToString() =
                x.File.Replace("\\", "/") + ":" +
                    (x.Line |> string) + ":" + 
                    x.MethodNamespace + "::" + x.MethodName


type CallLocation = ThisClass | SystemCall | ThisNamespace | ExternalCall

type TypeClass = String | Numeric | ClassType | Array | UndefinedType

type AtomKind = Expression | LocalVar | ClassField | MethodCall | Argument | Literal | ExternalField | ObjConstruction | Undefined

type Atom = {Text: string; Kind: AtomKind; DataType: TypeClass }
        with
            override x.ToString() =
                    x.Kind.ToString() + "[" + x.Text+ "] - " + x.DataType.ToString()


type Call = {MethodSymbol: ISymbol; MethodName: string; MethodNamespace: string; Location: CallLocation; Args: Atom list; ValArg: Atom option}
        with
            override x.ToString() =
                    x.MethodNamespace + "::" + x.MethodName + "(" + (x.ValArg |> string) + ") - " + x.Location.ToString()


type AssignmentSource = FromCall of Call | FromAtom of Atom | Empty

type AssignmentKind = Declaration | Local | Field | External

type DeclarationAssignment = {AssignmentKind: AssignmentKind; DataType: TypeClass; Source: AssignmentSource; Symbol: ISymbol option}
        with
            override x.ToString() =
                    "Assign " + (x.Symbol |> string) + " <" + x.DataType.ToString() + "> " + x.AssignmentKind.ToString() + " { " + x.Source.ToString() + " }" 

type Return = {Source: AssignmentSource;}
        with
            override x.ToString() =
                    "Return { " + x.Source.ToString() + " }" 


type IfStmt = {Source: AssignmentSource; HasElse: bool}
        with
            override x.ToString() =
                    "IF { " + x.Source.ToString() + " } else: " + x.HasElse.ToString() 

 
type LoopKind = ForLoop | ForeachLoop | WhileLoop
            
type Loop = { Kind: LoopKind; Value: AssignmentSource }
        with
            override x.ToString() =
                    "LOOP " + x.Kind.ToString() + "{ " + x.Value.ToString() + " }" 


type ConvertedItem = 
    | LoopNode of Loop
    | IfNode of IfStmt
    | ReturnNode of Return
    | DeclarationAssignmentNode of DeclarationAssignment
    | CallNode of Call
    | MethodRootNode of Method
    | UnknownNode
    | Special of string
    


let convertMethodDeclaration (s: SemanticModel) (m: MethodDeclarationSyntax) =
    let symbol = s.GetDeclaredSymbol(m)
    let name = symbol.Name
    let str = symbol.ToDisplayString()
    let ns = str.Remove(str.LastIndexOf("."))
    let line = ParseUtils.lineOf m
    let file = s.SyntaxTree.FilePath
    {MethodName = name; MethodSymbol = symbol; MethodNamespace = ns; Line = line; File = file}


let convertMethodDeclarationNode (s: SemanticModel) (m: MethodDeclarationSyntax) =
    MethodRootNode(convertMethodDeclaration s m)


let isNumericTypeName (s: string) =
    let numericTypes = [
        "Byte";
        "Int16";
        "Int32";
        "Int64";
        "SByte";
        "Uint16";
        "Uint32";
        "Uint64";
        "Decimal";
        "Double";
        "Single";
        ]
    List.contains s numericTypes


let typeInfoToTypeClass (t: TypeInfo) =
    let realType = match t.Type with 
        | null -> t.ConvertedType
        | _ -> t.Type
    if realType = null then ClassType else //just class type if type is unknown
        if (isNumericTypeName realType.Name) then Numeric else 
            match realType.TypeKind with
            | TypeKind.Array -> Array
            | TypeKind.Class -> if realType.Name = "String" then String else ClassType
            | TypeKind.Delegate -> ClassType
            | TypeKind.Interface -> ClassType
            | TypeKind.Struct -> ClassType
            | _ -> ClassType


let convertAtom (c: Context) (s: SemanticModel) (e: ExpressionSyntax) =
    if (e = null) then
        if ParseUtils.DEBUG_MODE then 
            printfn "convertAtom: no expression received"
        // default value
        {DataType = UndefinedType; Kind = Undefined; Text = "---"}
    else
        let t = s.GetTypeInfo(e)

        let typeinfo = typeInfoToTypeClass t

        let aKind =
            match e with
            | :? LiteralExpressionSyntax as lit -> Literal
            | :? IdentifierNameSyntax as id ->
                let sym = s.GetSymbolInfo(id).Symbol
                if sym = null then 
                    if ParseUtils.DEBUG_MODE then printfn "convertAtom: sym is null in %A" <| id.ToString()
                    Undefined
                else
                    match sym.Kind with
                    | SymbolKind.Local -> LocalVar
                    | SymbolKind.Field | SymbolKind.Property -> ClassField
                    | SymbolKind.Parameter -> Argument
                    | _ -> Expression
            | :? InvocationExpressionSyntax -> MethodCall
            | :? MemberAccessExpressionSyntax -> ExternalField
            | :? ElementAccessExpressionSyntax -> 
                LocalVar
            | :? ObjectCreationExpressionSyntax -> ObjConstruction
            | _ -> Expression

        {DataType = typeinfo; Kind = aKind; Text = e.ToString()}


let findMostValuableAtom (atoms: Atom list) =
    // Expression | LocalVar | ClassField | MethodCall | Argument | Literal | ExternalField
    let kinds = [MethodCall; ExternalField; Expression; ClassField; LocalVar; Argument; Literal]
    let rec compareTo listRem =
        match listRem with 
        | head :: tail -> 
            match (List.tryFind (fun (x: Atom) -> x.Kind = head) atoms) with
            | Some(x) -> Some(x)
            | None -> compareTo tail
        | [] -> None

    if List.isEmpty atoms then None else
        if List.length atoms = 1 then Some(atoms.[0]) else
            compareTo kinds

let convertInnerInvocExpression (c: Context) (s: SemanticModel) (m: InvocationExpressionSyntax) =
    let symbol = s.GetSymbolInfo(m).Symbol
    if symbol = null then 
        if ParseUtils.DEBUG_MODE then 
            printfn "convertInvocationExpression: Symbol in %A is null" <| m.ToString()
    let name = if symbol = null then "null" else symbol.Name
    let str = if symbol = null then "null" else symbol.ToDisplayString()
    let ns = if symbol = null then "null" else str.Remove(str.LastIndexOf("."))

    let isSymbolLocal = c.Namespace = ns
    let isSymbolSystem = ns.StartsWith("System.")
    let isSymbolThisNs = c.Namespace.StartsWith(ns.Split('.').[0])

    let calloc = 
        if isSymbolLocal then ThisClass else
            if isSymbolSystem then SystemCall else
                if isSymbolThisNs then ThisNamespace else ExternalCall

    let args = m.ArgumentList.Arguments |> Seq.toList

    let convertedArgs = List.map (fun (x: ArgumentSyntax) -> convertAtom c s x.Expression) args
    let mostValuableArg = findMostValuableAtom convertedArgs
    
    {MethodName = name; MethodSymbol = symbol; MethodNamespace = ns; 
        Location = calloc; Args = convertedArgs; ValArg = mostValuableArg}


let convertInvocationExpression (c: Context) (s: SemanticModel) (m: InvocationExpressionSyntax) =
    let inner = convertInnerInvocExpression c s m
    CallNode(inner)

   
let convertLocalDeclaration (c: Context) (s: SemanticModel) (m: LocalDeclarationStatementSyntax) =
    let declaration = m.Declaration
    
    let variable = declaration.Variables |> Seq.head //get only first declaration, ignore others
    let typeClass = typeInfoToTypeClass <| s.GetTypeInfo(declaration.Type)
    let symbol = s.GetDeclaredSymbol(variable)
    if (variable.Initializer = null) then
        DeclarationAssignmentNode({AssignmentKind = Declaration; DataType = typeClass; Symbol = Some(symbol); Source = Empty})
    else
        let inits = variable.Initializer.Value //init expression
        let value =
            match inits with
            | :? InvocationExpressionSyntax as i -> FromCall(convertInnerInvocExpression c s i)
            | _ -> FromAtom(convertAtom c s inits)
        DeclarationAssignmentNode({AssignmentKind = Declaration; DataType = typeClass; Symbol = Some(symbol); Source = value})

 
let convertAssignExpression (c: Context) (s: SemanticModel) (m: AssignmentExpressionSyntax) =
    let leftExpr = m.Left
    let typeClass = typeInfoToTypeClass <| s.GetTypeInfo(leftExpr)
    let mutable symbol = None

    let assKind =
        match leftExpr with
        | :? IdentifierNameSyntax as id ->
            let sym = s.GetSymbolInfo(id).Symbol
            if (sym = null) then Local else
                symbol <- Some(sym)
                match sym.Kind with
                | SymbolKind.Local -> Local
                | SymbolKind.Field | SymbolKind.Property -> Field
                | SymbolKind.Parameter -> Local
                | _ -> Local
        | :? MemberAccessExpressionSyntax -> External
        | :? ElementAccessExpressionSyntax as el -> 
            Local
        | _ -> External

    let rightExpr = m.Right
    let value =
        match rightExpr with
        | :? InvocationExpressionSyntax as i -> FromCall(convertInnerInvocExpression c s i)
        | _ -> FromAtom(convertAtom c s rightExpr)
    
    DeclarationAssignmentNode({AssignmentKind = assKind; DataType = typeClass; Symbol = symbol; Source = value})
    

let convertReturn (c: Context) (s: SemanticModel) (r: ReturnStatementSyntax) =
    let rightExpr = r.Expression
    let value =
        match rightExpr with
        | :? InvocationExpressionSyntax as i -> FromCall(convertInnerInvocExpression c s i)
        | _ -> FromAtom(convertAtom c s rightExpr)
    ReturnNode({ Source = value })


let convertIf (c: Context) (s: SemanticModel) (i: IfStatementSyntax) =
    let rightExpr = i.Condition
    let value =
        match rightExpr with
        | :? InvocationExpressionSyntax as i -> FromCall(convertInnerInvocExpression c s i)
        | _ -> FromAtom(convertAtom c s rightExpr)
    IfNode({ Source = value; HasElse = (i.Else = null |> not) })


let convertLoop (c: Context) (s: SemanticModel) (k) (e: ExpressionSyntax) =
    let value =
        match e with
        | :? InvocationExpressionSyntax as i -> FromCall(convertInnerInvocExpression c s i)
        | _ -> FromAtom(convertAtom c s e)
    LoopNode({Kind = k; Value = value})


let convertFor (c: Context) (s: SemanticModel) (i: ForStatementSyntax) =
    let expr = i.Condition
    convertLoop c s LoopKind.ForLoop expr


let convertForeach (c: Context) (s: SemanticModel) (i: ForEachStatementSyntax) =
    let expr = i.Expression
    convertLoop c s LoopKind.ForeachLoop expr


let convertWhile (c: Context) (s: SemanticModel) (i: WhileStatementSyntax) =
    let expr = i.Condition
    convertLoop c s LoopKind.WhileLoop expr

let convertUnknownNode (c: Context) (s: SemanticModel) (e: SyntaxNode) =
    UnknownNode