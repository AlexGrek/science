﻿module public Tools


open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open Microsoft.CodeAnalysis.MSBuild

let rec public traverseRead (func: SyntaxNode -> unit) (rot: SyntaxNode) =
    func rot |> ignore
    Seq.iter (traverseRead func) <| rot.ChildNodes()


let public traverseReadType (tp: Type) (func: SyntaxNode -> unit) =
    let filterexec (node: SyntaxNode) =
        if node.GetType().IsSubclassOf(tp) || node.GetType() = tp then
            func node
 
    traverseRead filterexec


let public traverseReadKind (kind: SyntaxKind) (func: SyntaxNode -> unit) =
    let filterexec (node: SyntaxNode) =
        if node.Kind() = kind then
            func node
 
    traverseRead filterexec


let public loadSolutionFile (file: string) (solution: string) =
    let msWorkspace = MSBuildWorkspace.Create();

    let projAsync = Async.AwaitTask <| msWorkspace.OpenProjectAsync(solution)
    let proj = Async.RunSynchronously projAsync
    let compilationAsync = Async.AwaitTask <| proj.GetCompilationAsync()
    let comp = Async.RunSynchronously compilationAsync
    //Seq.iter (printfn "%A") proj.Documents
    Seq.find (fun (el: Document) -> el.Name = file) proj.Documents

            
    


let public docRoot (doc: Document) =
    let projAsync = Async.AwaitTask <| doc.GetSyntaxRootAsync()
    Async.RunSynchronously projAsync


let public collectType<'T> (rot: SyntaxNode) = 
    ( Seq.filter (fun node -> node.GetType() = typeof<'T>) <| rot.ChildNodes() )
    |> Seq.toList

let public ofType<'T> (a: Object) =
    a.GetType().IsSubclassOf(typeof<'T>) || a.GetType() = typeof<'T>


let public traverseCollectType<'T> (rot: SyntaxNode) =
    let rec trav (root: SyntaxNode): SyntaxNode list =
        if ofType<'T> root then
            root :: (Seq.collect trav (root.ChildNodes()) |> Seq.toList)
        else 
            (Seq.collect trav (root.ChildNodes()) |> Seq.toList)
    trav rot


let public deepCollectType<'T> (rot: SyntaxNode) =
    let rec trav (root: SyntaxNode): SyntaxNode list =
        if ofType<'T> root then
            [root]
        else 
            (Seq.collect trav (root.ChildNodes()) |> Seq.toList)
    trav rot


let public deeperCollectType<'T> (rot: SyntaxNode) =
    (Seq.collect deepCollectType<'T> (if rot = null then Seq.empty else rot.ChildNodes()) |> Seq.toList)


let public getUniqueMethodName (m: MethodDeclarationSyntax) =
    let abs x = Math.Abs(x |> float) |> int
    let line = ParseUtils.lineOf m |> string
    let name = m.Identifier.ToString()
    let filehash = System.IO.Path.GetFileNameWithoutExtension(m.SyntaxTree.FilePath) |> hash |> abs |> string
    let mhash = if filehash.Length < 3 then filehash else filehash.Substring(0, 2)
    name + "_" + line + "_" + mhash


