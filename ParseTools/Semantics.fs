﻿module Semantics

open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open Microsoft.CodeAnalysis.MSBuild
open Convert

type Conversion =   None 
                  | CallSideEffectSystem 
                  | CallMethodLocal of string
                  | CallMethodExternal of string
                  | AssignMethodLocal of (string * string * TypeInfo)
                  | AssignMethodExternal of (string * string * TypeInfo)
                  | AssignMethodSystem of (string * string * TypeInfo)
                  | AssignExpression of string
                  | Increment of string
                  | Decrement of string
                  | ForLoop of string
                  | ForeachLoop of string
                  | Loop of string
                  | IfCondition of string

let getTypeOfExpression (sem: SemanticModel) (syn: SyntaxNode) =
    sem.GetTypeInfo(syn);

//let isSameMethod (sem: SemanticModel) (meth: MethodDeclarationSyntax) (call: Invoc)

//let buildInnerCfg (sem: SemanticModel) (meth: MethodDeclarationSyntax) =
//    let anal = sem.AnalyzeControlFlow(meth)

let createContext (sem: SemanticModel) (syn: SyntaxNode) =
    let toContext (method: Method) =
        {Method = method.MethodName; Namespace = method.MethodNamespace; ProjectPath = ""}
    match syn with
    | :? MethodDeclarationSyntax as m -> 
            (Convert.convertMethodDeclaration sem m) |> toContext
    | _ -> failwith <| "createContext called not with MethodDeclaration, but with " + syn.ToString()


let convert (context: Convert.Context) (sem: SemanticModel) (syn: SyntaxNode): ConvertedItem =
    if (syn = null) then 
        if ParseUtils.DEBUG_MODE then printfn "CONVERTING 'NULL' NODE"
        (Convert.convertUnknownNode context sem syn) 
    else
        let def = syn.ToString() + Environment.NewLine + (Print.treeStr (fun (x: SyntaxNode) -> x.GetType().ToString()) syn)

        let convertExpressionStatement (expr: ExpressionStatementSyntax) =
            let child = Seq.exactlyOne <| expr.ChildNodes()
            match child with 
            | :? InvocationExpressionSyntax as i -> 
                (Convert.convertInvocationExpression context sem i)
            | :? AssignmentExpressionSyntax as i -> 
                (Convert.convertAssignExpression context sem i)
            //| :? PostfixUnaryExpressionSyntax as i ->
            //    (Convert.convertPostfix context sem i)
            | _ ->
                if ParseUtils.DEBUG_MODE then printfn "UNCONVERTED EXPR: %A" <| child.ToString()
                (Convert.convertUnknownNode context sem child)

        match syn with
        | :? MethodDeclarationSyntax as m -> 
                (Convert.convertMethodDeclarationNode sem m)
        | :? ExpressionStatementSyntax as i -> 
                convertExpressionStatement i
        | :? LocalDeclarationStatementSyntax as l ->
                (Convert.convertLocalDeclaration context sem l)
        | :? ReturnStatementSyntax as ret -> 
                (Convert.convertReturn context sem ret)
        | _ -> 
            if ParseUtils.DEBUG_MODE then printfn "UNCONVERTED NODE: %A" <| syn.ToString()
            (Convert.convertUnknownNode context sem syn)



let convertEntry (context: Convert.Context) (sem: SemanticModel) (syn: SyntaxNode) =
    let def = syn.ToString() + Environment.NewLine + (Print.treeStr (fun (x: SyntaxNode) -> x.GetType().ToString()) syn)

    match syn with
    | :? IfStatementSyntax as m -> 
        (Convert.convertIf context sem m)
    | :? MethodDeclarationSyntax as m -> 
        (Convert.convertMethodDeclarationNode sem m)
    | :? WhileStatementSyntax as m -> 
        (Convert.convertWhile context sem m)
    | :? ForEachStatementSyntax as m -> 
        (Convert.convertForeach context sem m)
    | :? ForStatementSyntax as m -> 
        (Convert.convertFor context sem m)
    | _ -> 
        if ParseUtils.DEBUG_MODE then printfn "UNCONVERTED NODE: %A" <| syn.ToString()
        (Convert.convertUnknownNode context sem syn)