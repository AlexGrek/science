﻿module ParseUtils

open Microsoft.CodeAnalysis

let public lineOf (node: SyntaxNode) =
    node.GetLocation().GetLineSpan().StartLinePosition.Line + 1

let mutable public DEBUG_MODE = false
let mutable public COMPILE_ERRORS = true