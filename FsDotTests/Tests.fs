﻿module Tests

open System
open Xunit
open DotDump

type GraphNode = {id: string; links: string list} 

let a = {id = "a"; links = ["a"; "b"; "c"]}
let b = {id = "b"; links = ["a"]}
let c = {id = "c"; links = ["b"; "a"]}
let list = [a; b; c] |> List.toSeq
let getID (g: GraphNode) = g.id
let getFullText (g: GraphNode) = g.id + " - full text"
let getLinks (g: GraphNode) = g.links |> List.toSeq

[<Fact>]
let ``Simple Dump Test`` () =
    let graphUnit = generateGraph<GraphNode> getID getLinks None None
    let actual = printGraphToString graphUnit "testg" list
    let n = Environment.NewLine
    let expected = "digraph testg {" + n +
        n + "\"a\" -> \"a\"" +
        n + "\"a\" -> \"b\"" +
        n + "\"a\" -> \"c\"" +
        n + "\"b\" -> \"a\"" +
        n + "\"c\" -> \"b\"" +
        n + "\"c\" -> \"a\"" +
        n + "}" + n
    Assert.Equal(expected, actual)

[<Fact>]
let ``Simple Shape Test`` () =
    let getShape g = if g.id = "a" then Box else Default
    let graphUnit = generateGraph<GraphNode> getID getLinks None (Some(getShape))
    let actual = printGraphToString graphUnit "testg" list
    let n = Environment.NewLine
    let expected = "digraph testg {" +
        n + "\"a\" [shape=box]" +
        n + "\"a\" -> \"a\"" +
        n + "\"a\" -> \"b\"" + 
        n + "\"a\" -> \"c\"" +
        n + "\"b\" -> \"a\"" +
        n + "\"c\" -> \"b\"" +
        n + "\"c\" -> \"a\"" +
        n + "}" + n
    Assert.Equal(expected, actual)

[<Fact>]
let ``Shape and fulltext Test`` () =
    let getShape g = if g.id = "a" then Box else Default
    let graphUnit = generateGraph<GraphNode> getID getLinks (Some(getFullText)) (Some(getShape))
    let actual = printGraphToString graphUnit "testg" list
    let n = Environment.NewLine
    let expected = "digraph testg {" +
        n + "\"a\" [shape=box]" +
        n + "\"a\" [label=\"a - full text\"]" +
        n + "\"a\" -> \"a\"" +
        n + "\"a\" -> \"b\"" + 
        n + "\"a\" -> \"c\"" +
        n + "\"b\" [label=\"b - full text\"]" +
        n + "\"b\" -> \"a\"" +
        n + "\"c\" [label=\"c - full text\"]" +
        n + "\"c\" -> \"b\"" +
        n + "\"c\" -> \"a\"" +
        n + "}" + n
    Assert.Equal(expected, actual)

[<Fact>]
let ``Simple Write File Test`` () =
    let graphUnit = generateGraph<GraphNode> getID getLinks None None
    let n = Environment.NewLine
    let actual = System.IO.File.ReadAllText("testg.dot")
    let expected = "digraph testg {" + n +
        n + "\"a\" -> \"a\"" +
        n + "\"a\" -> \"b\"" +
        n + "\"a\" -> \"c\"" +
        n + "\"b\" -> \"a\"" +
        n + "\"c\" -> \"b\"" +
        n + "\"c\" -> \"a\"" +
        n + "}" + n
    Assert.Equal(expected, actual)

[<Fact>]
let ``Simple Generate Test`` () =
    let graphUnit = generateGraph<GraphNode> getID getLinks None None
    let actual = printGraphToFile graphUnit "testg.dot" "testg" list
    let n = Environment.NewLine
    DotGraph.makeSvg @".\testg.dot"
    let isFile = System.IO.File.Exists(@".\testg.svg")
    Assert.True(isFile)
