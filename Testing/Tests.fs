module TestsForCfg

open System
open Xunit

[<Fact>]
let ``Simple For Loop`` () =
    let cfg = CfgMain.buildCfgForMethodNameContainer "ForLoop" 
                (CfgMain.loadFileRootContainer "TestCases.cs" Notebook.solutionPath)
    let expectedString = "wtf?"
    Assert.Equal(expectedString, cfg.ShortPrint())
    
