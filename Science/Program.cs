﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Science
{
    class Program
    {
        static void Main(string[] args)
        {
            //A unit = new B();
            //Console.WriteLine("{0}", unit.inc());

            TaskKakoyto.A1();
            Console.WriteLine("AAA");

            TryCatch();
            foreach (string str in System.IO.File.ReadAllLines("INPUT.TXT"))
            {
                Console.WriteLine($"str: <{str}>");
            }


            Console.WriteLine("BBB");
            Console.ReadKey();
        }

        static int DoNothing(int i)
        {
            return i;
        }

        static int FIELD = 0;

        static void Calls(int arg1, string arg2)
        {
            var a = 4;
            Console.Write("X");
            Console.ReadKey();
            Math.Abs(23.4);
            Math.Pow(2, 4.4);
            Math.Round(Math.PI);
            Math.Round((double)arg1);
            Console.WriteLine(arg2);
            Console.Write(a);
            DoNothing(a);
            DoNothing(FIELD);
            DoNothing(Program.FIELD);
            DoNothing(DoNothing(FIELD));
            DoNothing(Program.DoNothing(FIELD));
            //DotDump.clearString("sdasff");
        }

        static void Assignment()
        {
            int a = 4;
            int b = DoNothing(a);
            a = 144;
            a = 325 + 345;
            a = b;
            int c = a;
            a = DoNothing(c);
            a++;
            int x;
            var pi = Math.PI;

            int[] i = new int[45];
            int[] d;
            d = new int[425];
            var s = new A();
            A aaa;
            aaa = new A();
            i[23] = a;
        }

        class A
        {
            int i = 0;

            public virtual int inc()
            {
                Console.WriteLine("A");
                return ++i;
            }
        }

        class B: A
        {
            int i = 10;

            public int inc()
            {
                Console.WriteLine("B");
                return ++i;
            }
        }

        private void SortArrayWithShellSort()
        {
            int[] array = { 297, 183, 464 };
            ShellSort(array);
        }


        private void ShellSort(int[] array)
        {
            int n = array.Length;
            int gap = n / 2;
            int temp;

            while (gap > 0)
            {
                for (int i = 0; i + gap < n; i++)
                {
                    int j = i + gap;
                    temp = array[j];

                    while (j - gap >= 0 && temp < array[j - gap])
                    {
                        array[j] = array[j - gap];
                        j = j - gap;
                    }

                    array[j] = temp;
                }

                gap = gap / 2;
            }
        }

        public static void CombSort(ref int[] data)
        {
            double gap = data.Length;
            bool swaps = true;

            while (gap > 1 || swaps)
            {
                gap /= 1.247330950103979;

                if (gap < 1)
                    gap = 1;

                int i = 0;
                swaps = false;

                while (i + gap < data.Length)
                {
                    int igap = i + (int)gap;

                    if (data[i] > data[igap])
                    {
                        int temp = data[i];
                        data[i] = data[igap];
                        data[igap] = temp;
                        swaps = true;
                    }

                    ++i;
                }
            }
        }

        public static List<int> BucketSort1(params int[] x)
        {
            List<int> result = new List<int>();

            if (result.Count > 0)
            {
                Console.WriteLine("MASS");

            }

            if (result.Count > 0)
                Console.WriteLine("MASS");

            if (result.Count > 0)
            {
                Console.WriteLine("MASS");
            }
            else
            {
                if (result.Count < 2)
                    Console.WriteLine("WTF");
                else
                    Console.Write("AAAGH");
            }

            if (result.Count > 0)
                Console.WriteLine("MASS");
            else
                Console.WriteLine("WTF");

            //Determine how many buckets you want to create, in this case, the 10 buckets will each contain a range of 10
            //1-10, 11-20, 21-30, etc. since the passed array is between 1 and 99
            int numOfBuckets = 10;

            //Create buckets
            List<int>[] buckets = new List<int>[numOfBuckets];
            for (int i = 0; i < numOfBuckets; i++)
                buckets[i] = new List<int>();

            //Iterate through the passed array and add each integer to the appropriate bucket
            NewMethod(x, numOfBuckets, buckets);

            //Sort each bucket and add it to the result List
            //Each sublist is sorted using Bubblesort, but you could substitute any sorting algo you would like
            for (int i = 0; i < numOfBuckets; i++)
            {
                int[] temp = BubbleSortList(buckets[i]);
                result.AddRange(temp);
            }
            return result;
        }

        private static void TryCatch()
        {
            int a = 2;
            try
            {
                a = 4;
                a = 5;
                if (a == 2)
                    throw new Exception();
                a = 6;
                a = 7;

            } catch(NullReferenceException e)
            {
                Console.WriteLine("NullReferenceException");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception EX");
            } finally
            {
                a = 5464564;
            }
            int b = 99;
        }

        private static void NewMethod(int[] x, int numOfBuckets, List<int>[] buckets)
        {
            for (int i = 0; i < x.Length; i++)
            {
                int buckitChoice = (x[i] / numOfBuckets);
                if (i > 3)
                    continue;
                buckets[buckitChoice].Add(x[i]);
            }
        }

        //Bubblesort w/ ListInput
        public static int[] BubbleSortList(List<int> input)
        {
            for (int i = 0; i < input.Count; i++)
            {
                for (int j = 0; j < input.Count; j++)
                {
                    if (input[i] < input[j])
                    {
                        int temp = input[i];
                        input[i] = input[j];
                        input[j] = temp;
                    }
                }
            }
            return input.ToArray();
        }

        public static object BinarySearchIterative(int[] inputArray, int key)
        {
            int min = 0;
            int max = inputArray.Length - 1;
            while (min <= max)
            {
                int mid = (min + max) / 2;
                if (key == inputArray[mid])
                {
                    return ++mid;
                }
                else if (key < inputArray[mid])
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }
            }
            return "Nil";
        }
    }
}
