﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Science
{
    class TestCases
    {
        void ForLoop()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i.ToString());
            }
        }

        void WhileLoop()
        {
            while(true)
            {
                Console.WriteLine("Endless");
            }
        }

        void ForContinue()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i.ToString());
                if (i == 5)
                    continue;
            }
        }

        void ForBreak()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                    break;
                Console.WriteLine(i.ToString());
            }
        }

        void IfThen()
        {
            int i = 4;
            if (i == 5)
            {
                i = 0;
            }
        }

        void IfThenElse()
        {
            int i = 4;
            if (i == 5)
            {
                i = 0;
            }
            else
                i = 2;
        }

        void SimpleBlock()
        {
            int i = 4;
            int x = 5;
            int y = 6;
            int z = 7;
        }

        void Empty() { }
    }
}
