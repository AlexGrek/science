﻿module HyperEncode
open System.Collections.Generic
open CfgConversion
open Convert
open Utils

type LineString(position: int, text: string) =
    override x.ToString() =
        let filler = String.replicate position " "
        filler + text
    member x.Length with get () = text.Length


type MethodEncode(methodUid: string) =
    let mutable CurrentPosition = 0
    let Lines = List<LineString>()

    member this.AddLine s =
        Lines.Add(s)
   

let encodeSimpleNode (n: ConvertedItem) =
    match n with
    | LoopNode(lp) -> 
        let kind = Encode.encLoopKind lp.Kind |> string
        let value = Encode.encAssignmentSource lp.Value
        kind + value
    | IfNode (node) ->
        let core = if node.HasElse then "<" else ">" 
        let value = Encode.encAssignmentSource node.Source
        core + value
    | ReturnNode(node) ->
        "=" + Encode.encAssignmentSource node.Source
    | DeclarationAssignmentNode(das) ->
        let kind = Encode.encAssKind das.AssignmentKind |> string
        let value = Encode.encAssignmentSource das.Source
        kind + value
    | CallNode(c) -> 
        let location = Encode.encCallLocation c.Location |> string
        "$" + location + (Encode.encSomeData c.ValArg) + "$"
    | MethodRootNode(root) ->
        root.MethodName
    | UnknownNode -> "????"
    | Special(s) -> "~" + s.Substring(3)


let hyperDotConf =
    let getID (g: CfgListNode) = g.uid
    let getFullText (g: CfgListNode) = g.uid + " | " + (encodeSimpleNode g.conversion)
    let getLinks (g: CfgListNode) = g.children |> List.toSeq
    DotDump.generateGraph<CfgListNode> getID getLinks (Some(getFullText)) None

