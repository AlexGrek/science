﻿module Cfg

open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open Tools
open Log

type CfgNodeContent = 
    | Syntax of SyntaxNode 
    | Pseudo of string 
    | Entry of SyntaxNode 
    | Exit of SyntaxNode

type CfgNode = { node: CfgNodeContent; mutable links: CfgNode list; }
    with 
        member this.getUID() = 
            let syntax = this.node
            let text =
                match syntax with
                | Syntax s -> sprintf "\"%s\"" (Print.detailsToString s)
                | Pseudo p -> sprintf "\"%s\"" p
                | Entry n -> sprintf "\"ENTRY(%s)\"" (Print.detailsToString n)
                | Exit  n -> sprintf "\"EXIT(%s)\""  (Print.detailsToString n)
            text
        override this.ToString() =
            this.getUID()
        member x.ShortPrint() =
            let rec sprint level visited node =
                let str = node.ToString()
                let data = List.map (fun x -> sprintf "(%d) %s -> %s" level str (x.ToString())) node.links
                        |> List.reduce (fun acc el -> acc + "\n" + el)
                data + "\n" +
                    (List.filter (fun (x: CfgNode) -> not <| List.contains (x.getUID()) visited) node.links
                    |> List.map (sprint (level+1) ((node.getUID()) :: visited))
                    |> List.reduce (fun acc el -> acc + "\n--\n" + el))
            sprint 0 [] x
        

type TaggedCfgNode = {cfgn: CfgNode; tag: obj }


type CfgContext = { methodEnd: CfgNode; endNode: CfgNode; loopStart: CfgNode; loopEnd: CfgNode; catch: CfgNode seq }

let spawnEntryNode snode =
    { node = Entry(snode); links = [] }

let spawnExitNode snode exitLink =
    { node = Exit(snode); links = [ exitLink ] }

let spawnExitNodeWithLinks snode links =
    { node = Exit(snode); links = links }

let appendLink cnode link =
    //printfn "connecting %s to %s" (getUID cnode) (getUID link)
    { cnode with links = List.append cnode.links [ link ] }

let appendLinks cnode linkList =
    //printfn "connecting %s to %A" (getUID cnode) (List.map getUID linkList)
    { cnode with links = List.append cnode.links linkList }

let mutAppendLinks cnode linkList =
    //printfn "Trying to append...."
    let bak = List.map (fun x -> x) cnode.links
    cnode.links <- bak @ linkList
    //printfn "Append completed!"

let mutAppendLink cnode link =
    mutAppendLinks cnode [ link ]

let prependLink cnode link =
    { cnode with links = link :: cnode.links }

let prependLinks cnode linkList =
    { cnode with links = List.append linkList cnode.links }

let rec processNode (context: CfgContext) (node: SyntaxNode) =
    let processBlock (block: BlockSyntax) =
        let entry = spawnEntryNode block
        let exit = spawnExitNode block context.endNode
        let children = (block.ChildNodes() |> Seq.toList |> List.rev)
        let processing acc item =
            let newcontext = {context with endNode = acc}
            processNode newcontext item
        {entry with links = [ List.fold processing exit children ]}

    let deeper node innerNode =
        let entry = spawnEntryNode node
        let exit = spawnExitNode node context.endNode
        let nc = {context with endNode = exit}
        let statements = processNode nc innerNode
        appendLink entry statements

    let processSingleStatement (stmt: SyntaxNode) =
        let innerStmt = deeperCollectType<StatementSyntax> stmt
        match innerStmt with
        | [one] -> 
            INFO <| sprintf "Unknown statement %s has one inner statement." 
                        (Print.detailsOf stmt)
            deeper stmt one
        | [] ->
            { node = Syntax(stmt); links = [ context.endNode ] }
        | one::_ ->
            WARNING <| sprintf "Statement %s has more than one inner statements: %d." 
                        (Print.detailsOf stmt) (List.length innerStmt)
            deeper stmt one

    let processElse (elses: ElseClauseSyntax) =
        let entry = spawnEntryNode elses
        let exit = spawnExitNode elses context.endNode
        let nc = {context with endNode = exit}
        let statements = processNode nc elses.Statement
        appendLink entry statements

    let processIf (ifs: IfStatementSyntax) =
        let entry = spawnEntryNode ifs
        let exit = spawnExitNode ifs context.endNode
        let nc = {context with endNode = exit}
        let thenClause = processNode nc ifs.Statement
        let elses = if ifs.Else = null then exit else processNode nc ifs.Else
        appendLinks entry [thenClause; elses]

    let processFor (fors: ForStatementSyntax) =
        let body = fors.Statement
        let entry = spawnEntryNode fors
        let exit = spawnExitNodeWithLinks fors [context.endNode; entry]
        let nc = {context with endNode = exit; loopStart = entry; loopEnd = exit }
        let processedBody = processNode nc body
        mutAppendLinks entry [processedBody; exit]
        entry

    let processForEach (fors: ForEachStatementSyntax) =
        let body = fors.Statement
        let entry = spawnEntryNode fors
        let exit = spawnExitNodeWithLinks fors [context.endNode; entry]
        let nc = {context with endNode = exit; loopStart = entry; loopEnd = exit }
        let processedBody = processNode nc body
        mutAppendLinks entry [processedBody; exit]
        entry

    let processWhile (w: WhileStatementSyntax) =
        let body = w.Statement
        let entry = spawnEntryNode w
        let exit = spawnExitNodeWithLinks w [context.endNode; entry]
        let nc = {context with endNode = exit; loopStart = entry; loopEnd = exit }
        let processedBody = processNode nc body
        mutAppendLinks entry [processedBody; exit]
        entry

    let processContinue (cont: ContinueStatementSyntax) =
        { node = Syntax(cont); links = [ context.loopStart ] }

    let processBreak (brk: BreakStatementSyntax) =
        { node = Syntax(brk); links = [ context.loopEnd ] }

    let processThrow (t: ThrowStatementSyntax) =
        let target =
            if Seq.isEmpty context.catch 
            then context.methodEnd
            else Seq.head context.catch //stupid selection only, must be more complex
        { node = Syntax(t); links = [ target ] }

    let processTry (t: TryStatementSyntax) =
        let body = t.Block
        let entry = spawnEntryNode t
        let exit = spawnExitNode t context.endNode
        let catches = t.Catches
        let final = t.Finally
        let nc = {context with endNode = exit; } //throw is ignored by now
        let processedCatches = Seq.map (processNode nc) catches
        let ncTry = { nc with catch = processedCatches }
        let processedBody = processNode ncTry body
        if not (final = null) then
            let finalContext = { nc with endNode = context.endNode }
            exit.links <- [ processNode finalContext final ]
        { entry with links = [ processedBody ] }
        

    match node with
    | :? BlockSyntax as block -> processBlock block
    | :? IfStatementSyntax as ifs -> processIf ifs
    | :? ElseClauseSyntax as elses -> processElse elses
    | :? ForStatementSyntax as fors -> processFor fors
    | :? ForEachStatementSyntax as fors -> processForEach fors
    | :? ContinueStatementSyntax as cont -> processContinue cont
    | :? BreakStatementSyntax as brk -> processBreak brk
    | :? WhileStatementSyntax as wh -> processWhile wh
    | :? TryStatementSyntax as tr -> processTry tr
    | :? ThrowStatementSyntax as thr -> processThrow thr
    | _ -> processSingleStatement node

let public createCfgForMethod (root: MethodDeclarationSyntax) =
    let methodEndNode = { node = Pseudo("End of method"); links = [] }
    let methodStartNode = spawnEntryNode root
    let initialContext = {
        methodEnd = methodEndNode;
        endNode = methodEndNode;
        loopStart = methodStartNode; // :continue: will link here
        loopEnd = methodEndNode // :break: will link here
        catch = Seq.empty
        }
    let entry = processNode initialContext root.Body
       
    appendLink methodStartNode entry

