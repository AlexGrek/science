﻿open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open System.Diagnostics
open System.IO

let mutable MS_COUNT: int64 = 0L

let runBenchSeq() = 
    Log.SUCCESS "Sequential processing:"
    let stopwatch = Stopwatch()
    stopwatch.Start()
    let cf = CfgMain.loadFileRoot "Program.cs" Notebook.solutionPath
    let graphs = CfgMain.buildCfgForAllMethods cf.Root
                    |> CfgMain.convertCfgAllPairs cf
    FileIO.makeAllSvgs HyperEncode.hyperDotConf graphs
    stopwatch.Stop()
    Log.SUCCESS (sprintf "Result: %d" stopwatch.ElapsedMilliseconds)
    MS_COUNT <- MS_COUNT + stopwatch.ElapsedMilliseconds


let cfgForFileSeq solution file =
    let cf = CfgMain.loadFileRoot file solution
    let graphs = CfgMain.buildCfgForAllMethodsParallel cf.Root
                    |> CfgMain.convertCfgAllPairs cf
    FileIO.makeAllSvgsParallel CfgConversion.convertedCfgDotEncodingConf graphs


let cfgForFilePar solution file =
    let cf = CfgMain.loadFileRoot file solution
    let graphs = CfgMain.buildCfgForAllMethods cf.Root
                    |> CfgMain.convertCfgAllPairs cf
    FileIO.makeAllSvgs CfgConversion.convertedCfgDotEncodingConf graphs


let cfgForFileProjPar proj file =
    Log.SUCCESS <| sprintf "Creating CFG for file %s" file
    let cf = CfgMain.loadFileFromProject file proj
    let graphs = CfgMain.buildCfgForAllMethodsParallel cf.Root
                    |> CfgMain.convertCfgAllPairsParallel cf

    //FileIO.makeAllDots CfgConversion.convertedCfgDotEncodingConf graphs
    FileIO.makeAllDots DumpGraph.blockEncodingConf graphs


let runBenchPar() = 
    Log.SUCCESS "Parallel processing:"
    let stopwatch = Stopwatch()
    stopwatch.Start()
    let cf = CfgMain.loadFileRoot "Program.cs" Notebook.solutionPath
    let graphs = CfgMain.buildCfgForAllMethodsParallel cf.Root
                    |> CfgMain.convertCfgAllPairs cf
    FileIO.makeAllSvgsParallel CfgConversion.convertedCfgDotEncodingConf graphs
    stopwatch.Stop()
    Log.SUCCESS (sprintf "Result: %d" stopwatch.ElapsedMilliseconds)
    MS_COUNT <- MS_COUNT + stopwatch.ElapsedMilliseconds


let benchmark() =
    for i in [0..10] do
        runBenchSeq()
    Log.SUCCESS (sprintf "Total sequential: %d" MS_COUNT)
    let seq = MS_COUNT
    MS_COUNT <- 0L
    for i in [0..10] do
        runBenchPar()
    let par = MS_COUNT
    Log.SUCCESS (sprintf "Total parallel: %d (speedup: %A)" MS_COUNT (float(seq) / float(par)))


let buildCfg() =
    FileIO.changeFilePath  <| Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "CFG")
    //benchmark()
    let path = Notebook.solutionPath
    let file = "Program.cs"
    //let file = @"CParserImpl.cs"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\CLanguage\CLanguage\CLanguage.csproj"
    cfgForFilePar path file
    Console.ReadKey()


let buildCfgForProject() =
    FileIO.changeFilePath  <| Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "CFG")
    let path = Notebook.solutionPath

    // init log and stat
    Log.setStatPath <| Path.Combine(FileIO.FILE_PATH, "stat.csv")
    Log.setLogPath <| Path.Combine(FileIO.FILE_PATH, "log.txt")
    Log.printStatMessage "file,method,nodes,entrys,exits,normals,loc"

    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\dafny\Source\DafnyRuntime\DafnyRuntime.csproj"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\dafny\Source\Dafny"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\shadowsocks-windows\shadowsocks-csharp"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\OrlovaCoursework\Orloff"
    //let path = @"C:\Users\AlexG\source\repos\Science\CloneDemo"
    let proj = CodeProject.loadProject path
    let files = proj.GetAllDocumentNames() |> Seq.toList
    Log.SUCCESS "Files detected:"
    List.iter (fun x -> Log.INFO <| sprintf "File %s" x) files
    let stopwatch = Stopwatch()
    stopwatch.Start()
    List.iter (cfgForFileProjPar proj) files
    stopwatch.Stop()
    Log.SUCCESS <| sprintf "Completed. Elapsed time: %As" stopwatch.Elapsed.TotalSeconds
    Console.ReadKey()


let buildCfgForProjectSol() =
    FileIO.changeFilePath  <| Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "CFG")
    let path = @"C:\Users\AlexG\Documents\dev\science\Science\Sience.sln"
    let projname = "Science"

    // init log and stat
    Log.setStatPath <| Path.Combine(FileIO.FILE_PATH, "stat.csv")
    Log.setLogPath <| Path.Combine(FileIO.FILE_PATH, "log.txt")
    Log.printStatMessage "file,method,nodes,entrys,exits,normals,loc"

    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\dafny\Source\DafnyRuntime\DafnyRuntime.csproj"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\dafny\Source\Dafny"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\shadowsocks-windows\shadowsocks-csharp"
    //let path = @"C:\Users\AlexG\Documents\dev\SAmplesForCFG\OrlovaCoursework\Orloff"
    let proj = CodeProject.loadProjectSolution path projname
    let files = proj.GetAllDocumentNames() |> Seq.toList
    Log.SUCCESS "Files detected:"
    List.iter (fun x -> Log.INFO <| sprintf "File %s" x) files
    let stopwatch = Stopwatch()
    stopwatch.Start()
    List.iter (cfgForFileProjPar proj) files
    stopwatch.Stop()
    Log.SUCCESS <| sprintf "Completed. Elapsed time: %As" stopwatch.Elapsed.TotalSeconds
    Console.ReadKey()


[<EntryPoint>]
let main argv = 
    //Notebook.main4()
    buildCfgForProject()
    //buildCfg()
    Console.ReadKey()
    0 // return an integer exit code


    //-		[2]	{"Could not load file or assembly 'Microsoft.Build, Version=15.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a' or one of its dependencies. The located assembly's manifest definition does not match the assembly reference. (Exception from HRESULT: 0x80131040)":"Microsoft.Build, Version=15.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"}	System.Exception {System.IO.FileLoadException}

//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.Framework.dll"
//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.dll"
//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.Engine.dll"
//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.Conversion.Core.dll"
//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.Tasks.Core.dll"
//gacutil /i "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\Microsoft.Build.Utilities.Core.dll"
