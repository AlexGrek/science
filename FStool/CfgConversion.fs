﻿module CfgConversion

open Cfg
open Microsoft.CodeAnalysis
open CodeProject
open Utils
open Log
open Microsoft.CodeAnalysis.CSharp.Syntax
open DotDump
open System.Collections.Generic
open System.Collections.Generic
open CfgRefinement

type CfgListNode = { node: CfgNode; uid: string; conversion: Convert.ConvertedItem; children: string list; parents: string list }

type ConvertedGraph = { nodes: CfgListNode list; index: Dictionary<string, CfgListNode> }
        with
            member this.GetNode (s: string) =
                this.index.[s]

            member this.GetNodeChildren (s: string) =
                this.index.[s].children

            member this.GetNodeParents (s: string) =
                this.index.[s].parents


let getMethodNameFromRootNode (root: CfgNode) =
    match root.node with
    | Entry(m) -> 
        let decl = m :?> MethodDeclarationSyntax
        Tools.getUniqueMethodName decl
    | _ -> "<< NO METHOD NAME >>"


let collectStats (cfile: CodeFile) (root: CfgNode) (nodes: CfgListNode seq) =
    (* stat csv format: *)
    (* file,method,nodes,entrys,exits,normals *)
    let isEntry (node: CfgListNode) =
        match node.node.node with
        | Entry(_) -> true
        | _ -> false
    let isExit (node: CfgListNode) =
        match node.node.node with
        | Exit(_) -> true
        | _ -> false
    let isSyntax (node: CfgListNode) =
        match node.node.node with
        | Syntax(_) -> true
        | _ -> false
    let fname = System.IO.Path.GetFileName(cfile.Semantic.SyntaxTree.FilePath)
    let method = getMethodNameFromRootNode root
    let entrys = Seq.filter isEntry nodes |> Seq.length
    let exits = Seq.filter isExit nodes |> Seq.length
    let normals = Seq.filter isSyntax nodes |> Seq.length
    let nodescount = Seq.length nodes
    printStatMessage 
        <| sprintf "%s,%s,%d,%d,%d,%d,%d" 
            fname 
            method 
            nodescount 
            entrys 
            exits 
            normals
            (cfile.LOC())
    

let convert (cfile: CodeFile) (root: TaggedCfgNode) =
    let parentage = root.tag :?> Dictionary<string, CfgRefinedNode> //optimized
    //let parentage = CfgRefinement.restoreParentage root.cfgn
    let context = 
        match root.cfgn.node with
            | Entry s -> Semantics.createContext cfile.Semantic s
            | _ -> failwith "root node for convert is not ENTRY node"
    let nodeList = CfgRefinement.getNodesList root.cfgn
    //printfn "SEMANTIC: %A" cfile.Semantic.Compilation.Assembly 
    let convertSyntax (node: CfgNode) =
        match node.node with
        | Syntax s -> Semantics.convert context cfile.Semantic s
        | Pseudo s -> Convert.Special(s)
        | Entry s -> Semantics.convertEntry context cfile.Semantic s
        | Exit s -> Semantics.convertEntry context cfile.Semantic s
    let convertingList = System.Collections.Generic.Dictionary<string, CfgListNode>()
    let convertNode (node: CfgNode) =
        let children = List.map getUID node.links
        let uid = getUID node
        convertingList.Add(uid, {
            uid = uid; node = node; 
            children = children; 
            conversion = convertSyntax node;
            parents = List.map getUID parentage.[uid].parents
            })
    Seq.iter convertNode <| nodeList.Values
    
    if Configuration.COLLECT_STATS then
        collectStats cfile root.cfgn convertingList.Values
    let nodes = convertingList.Values |> Seq.toList
    {nodes = nodes; index = convertingList}



let convertedCfgDotEncodingConf =
    let getID (g: CfgListNode) = g.uid
    let getFullText (g: CfgListNode) = g.uid + " | " + (g.conversion |> string)
    let getLinks (g: CfgListNode) = g.children |> List.toSeq
    DotDump.generateGraph<CfgListNode> getID getLinks (Some(getFullText)) None

let dumpConvertedCfg (data: CfgListNode list) name path =
    DotDump.printGraphToFile convertedCfgDotEncodingConf path name data


