﻿module public CfgMain

open Microsoft.CodeAnalysis.CSharp.Syntax
open Microsoft.CodeAnalysis
open CodeProject
open ParseUtils

type SyntaxNodeContainer = { syn: SyntaxNode }

let public loadFileRoot fileName solutionPath =
    let proj = CodeProject.loadProject solutionPath
    let docs = proj.MustGetFileByName fileName
    docs

let public loadFileFromProject fileName (proj: CodeProject.CodeProject) =
    let docs = proj.MustGetFileByName fileName
    docs

let public loadFileRootContainer fileName solutionPath =
    let syn =Tools.loadSolutionFile fileName solutionPath 
            |> Tools.docRoot
    {syn = syn}


let public getMethods root =
    List.map (fun (snode: SyntaxNode) -> 
            let method = snode :?> MethodDeclarationSyntax
            (Tools.getUniqueMethodName method, method))
    <| (Tools.traverseCollectType<MethodDeclarationSyntax> root)


let public getMethodsContainer root =
    List.map (fun (snode: SyntaxNode) -> 
            let method = snode :?> MethodDeclarationSyntax
            (method.Identifier.ToString(), {syn = method}))
    <| (Tools.traverseCollectType<MethodDeclarationSyntax> root.syn)


let public buildCfgForMethod (node: MethodDeclarationSyntax) =
    Cfg.createCfgForMethod node
    |> CfgRefinement.refineCFG


let public buildCfgForMethodContainer (node: SyntaxNodeContainer) =
    Cfg.createCfgForMethod (node.syn :?> MethodDeclarationSyntax)
    |> CfgRefinement.refineCFG


let public buildCfgForMethodPair (_, node) =
    buildCfgForMethod node


let public buildCfgForMethodName name root =
    let matched = List.filter (fun x -> (fst x) = name) (getMethods root)
    if List.length matched > 1 then
        invalidArg "name" <| sprintf "%d methods with name %s found, 1 expected" 
                        (List.length matched) name
    if List.length matched < 1 then
        invalidArg "name" <| sprintf "no methods with name %s found" name
    buildCfgForMethodPair matched.[0]


let public buildCfgForMethodNameContainer name container =
    buildCfgForMethodName name (container.syn)


let public buildCfgForAllMethods: SyntaxNode -> (string * Cfg.TaggedCfgNode) list =
    List.map (fun x -> (fst x, buildCfgForMethodPair x))
    << getMethods

let public buildCfgForAllMethodsParallel (root: SyntaxNode) : (string * Cfg.TaggedCfgNode) list =
     List.toArray (getMethods root)
     |> Array.Parallel.map (fun x -> (fst x, buildCfgForMethodPair x))
     |> Array.toList

    
let public dotCfgPair (name, cfg) =
    Dot.printCfg name cfg
    

let public dotCfgAllPairs =
    List.map (fun (name, cfg) -> (name, Dot.printCfg name cfg))


let public convertCfgAllPairs codeFile =
    List.map (fun (name, cfg) -> (name, (CfgConversion.convert codeFile cfg).nodes))


let public convertCfgAllPairsParallel codeFile pairs =
    let arr = List.toArray pairs
    let convert cfg = 
        let shrinked = (CfgConversion.convert codeFile cfg) |> DumpGraph.shrink
        shrinked.Values |> Seq.toList
    Array.Parallel.map (fun (name, cfg) -> 
                        (name, 
                            convert cfg))
                        arr
    |> Array.toList
