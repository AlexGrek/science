﻿module DumpGraph

open CfgConversion
open System.Collections.Generic 
open System.Collections.Generic

//let multiways (g: ConvertedGraph) = 
//    let visited = Set<CfgListNode>()
//    let movesTaken = Dictionary<string, int>()
//    let onePath graph =
//        match graph.children with
//        | [ x ] -> true
//        | _ -> false
//'   let rec traverse node str = 
//        if (onePath node) then

//    let root = g.nodes.Head

type BlockNode = {id: string; nodes: CfgListNode list; childrens: string list}


// CfgListNodes -> BlockNodes
let shrink (g: ConvertedGraph) =
    let getItem id =
        g.index.[id]
    let root = g.nodes.Head
    let blocks = Dictionary<string, BlockNode>()
    let visited = HashSet<string>()
    let isLinear node =
        (node.children.Length = 1) && (node.parents.Length = 1)
    let canBeHead node =
        (node.children.Length = 1)
    let canBeTail node =
        (node.parents.Length = 1)
    let rec traverse (inside: CfgListNode list) (node: CfgListNode): unit =
        //printfn "--> Visiting %A\nState: %A\n\n" node.uid inside
        if not (visited.Contains(node.uid)) then
            visited.Add(node.uid) |> ignore
            match inside with
            | [] -> // we are outside block
                if (canBeHead node) then
                    // begin new block
                    //printfn "New block: %s;\n, so lets traverse next 1 child: %s" node.uid node.children.Head
                    traverse [node] (getItem node.children.Head)
                else
                    // create block with single node
                    let single = {nodes = [ node ]; id = node.uid; childrens = node.children}
                    // push it
                    //printfn "Single block: %s" node.uid
                    blocks.Add(node.uid, single)
                    List.iter (fun x -> traverse [] (getItem x)) node.children
            | head::_ -> // we are inside block
                if (isLinear node) && (canBeTail <| getItem node.children.Head) then
                    // add item to block
                    traverse (inside@[node]) (getItem node.children.Head)
                else //tail is inside
                    // finalize block creation
                    let last = List.last inside
                    let block = {nodes = inside; id = head.uid; childrens = last.children}
                    blocks.Add(head.uid, block)
                    //create new single block for tail
                    let single = {nodes = [ node ]; id = node.uid; childrens = node.children}
                    blocks.Add(node.uid, single)
                    //printfn "I'm at %A\nNext I will dump: %A" node.uid node.children
                    List.iter (fun x -> traverse [] (getItem x)) node.children
        else 
            if not <| List.isEmpty inside then
                // finalize block creation
                    let last = List.last inside
                    let block = {nodes = inside; id = inside.Head.uid; childrens = last.children}
                    blocks.Add(inside.Head.uid, block)
        //    printfn "Loop detected: %A" node
    traverse [] root
    blocks


let blockEncodingConf =
    let getID (g: BlockNode) = g.id
    let getFullText (g: BlockNode) = (g.childrens.Length |> string) +
            (List.fold (fun acc el -> acc + "\n////////////\n" + el.node.ToString() + "\n######\n" + el.parents.ToString()) "" g.nodes)
    let getLinks (g: BlockNode) = g.childrens |> List.toSeq
    DotDump.generateGraph<BlockNode> getID getLinks (Some(getFullText)) None


let dumpToDot (data: BlockNode list) name path =
    DotDump.printGraphToFile blockEncodingConf path name data
