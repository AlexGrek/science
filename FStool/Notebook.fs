﻿module Notebook

open Microsoft.CodeAnalysis.CSharp
open Microsoft.CodeAnalysis
open Microsoft.CodeAnalysis.CSharp.Syntax
open System
open Microsoft.CodeAnalysis.MSBuild
open Tools;
open Print;
open Utils
open CfgMain

//let root = tree.GetRoot()

//Seq.iter (fun (s: SyntaxNode) -> printfn "Child: %A" s) <| root.ChildNodes()

//let rec traverseRead (func: SyntaxNode -> unit) (rot: SyntaxNode) =
//    func rot |> ignore
//    Seq.iter (traverseRead func) <| rot.ChildNodes()

//let traverseReadType (tp: Type) (func: SyntaxNode -> unit) =
//    let filterexec (node: SyntaxNode) =
//        if node.GetType() = tp then
//            func node
 
//    traverseRead filterexec

//let traverseReadKind (kind: SyntaxKind) (func: SyntaxNode -> unit) =
//    let filterexec (node: SyntaxNode) =
//        if node.Kind() = kind then
//            func node
 
//    traverseRead filterexec

//traverseRead (fun x -> printfn "any: %A %A %A" (x.Kind()) (x.GetType()) x) root

//traverseReadType (typeof<IfStatementSyntax>) (fun x -> printfn "if: %A %A" (x.Kind()) x) root

//traverseReadKind (SyntaxKind.ExpressionStatement) (fun x -> printfn "expression: %A %A" (x.Kind()) x) root


// solutions

//let solutionPath = @"C:\Users\AlexG\source\repos\Science\Science\Science.csproj";
let solutionPath =
    if System.IO.File.Exists(@"C:\Users\AlexG\Documents\dev\science\Science\Science.csproj") 
        then @"C:\Users\AlexG\Documents\dev\science\Science\Science.csproj" 
        else @"C:\Users\AlexG\source\repos\Science\Science\Science.csproj"
//let msWorkspace = MSBuildWorkspace.Create();

//let projAsync = Async.AwaitTask <| msWorkspace.OpenProjectAsync(solutionPath)
//let proj = Async.RunSynchronously projAsync


//Seq.iter (fun (d: Document) -> printfn "%A - %A" d.Name d.SourceCodeKind) proj.Documents

//let collectMethods = Tools.collectType<MethodDeclarationSyntax>

//let collectIfs = Tools.collectType<IfStatementSyntax> 

//let collectStatements = Tools.traverseCollectType<StatementSyntax>

//Tools.traverseReadType (typeof<MethodDeclarationSyntax>) 
//    (fun x -> printfn "if: %A %A" (x.Kind()) (x.Parent.Kind())) doc

//let main() = 
//    let doc = loadFileRoot "Program.cs" solutionPath
//    List.map Tools.deepCollectType<MethodDeclarationSyntax>  <|Tools.traverseCollectType<MethodDeclarationSyntax> doc
//    |> List.concat
//    |> List.iter printDetails

//let main3() = 
//    let doc = loadFileRoot "Program.cs" solutionPath
//    List.map Tools.deepCollectType<InvocationExpressionSyntax>  <|Tools.traverseCollectType<InvocationExpressionSyntax> doc
//    |> List.concat
//    |> List.iter (fun (x: SyntaxNode) -> let met = x :?> InvocationExpressionSyntax
//                                         printfn "%A" <| met)

let main4() =
    let proj = CodeProject.loadProject solutionPath
    Seq.iter (fun x -> printfn "%A" x) <| proj.GetAllDocumentNames()
    let docs = proj.MustGetFileByName "Program.cs"
    List.map Tools.deepCollectType<ExpressionSyntax>  <| Tools.traverseCollectType<MethodDeclarationSyntax> (docs.Root)
    |> List.concat
    |> List.iter (fun x -> printDetails x
                           printfn "%A" <| (docs.GetTypeOfExpression x).Type)

//let main2() =
//    let doc = loadFileRoot "Program.fs" solutionPath
//    let cfg = (Tools.traverseCollectType<MethodDeclarationSyntax> doc).[4] :?> MethodDeclarationSyntax
//            |> Cfg.createCfgForMethod
//    let parentage = CfgRefinement.restoreParentage cfg
//    let printParentage (n: CfgRefinement.CfgRefinedNode) =
//        printfn "--- %s:" (getUID n.node)
//        List.iter (fun x -> printfn "    %s" <| getUID x) n.parents
//    Seq.iter printParentage parentage.Values
//    CfgRefinement.refineCFG cfg
//    |> Dot.printCfg "aaa"
