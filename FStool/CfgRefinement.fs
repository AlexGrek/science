﻿module CfgRefinement

open Cfg
open Utils
open Microsoft.CodeAnalysis.CSharp.Syntax
open System.Collections.Generic
open ParseUtils

type CfgRefinedNode = { node: CfgNode; mutable parents: CfgNode list }

let visited = System.Collections.Generic.HashSet<string>()

let getNodesList (root: CfgNode) =
    let collectionOfNodes = System.Collections.Generic.Dictionary<string, CfgNode>()
    let rec traverse (node: CfgNode) =
        if not (collectionOfNodes.ContainsKey(getUID node)) then
            collectionOfNodes.Add(getUID node, node) |> ignore
            List.iter traverse node.links
    
    traverse root
    collectionOfNodes


let lineOfNode (c: CfgNode) =
    match c.node with
    | Entry(x) -> lineOf x
    | Exit(x) -> lineOf x
    | Pseudo(s) -> -1
    | Syntax(q) -> lineOf q


let restoreParentage (root: CfgNode) =
    let nodeList = getNodesList root
    let parents = System.Collections.Generic.Dictionary<string, CfgRefinedNode>()
    let reParent (node: CfgNode) =
        let filterLinkedTo target (item: CfgNode) =
            let name = getUID target
            let links = List.map getUID item.links
            List.contains name links
        let parentsFound = Seq.filter (filterLinkedTo node) nodeList.Values
                        |> Seq.toList
        parents.Add(getUID node, {node = node; parents = parentsFound})
    Seq.iter reParent nodeList.Values
    parents
        

let rec removeNodeWhen (parentages: Dictionary<string, CfgRefinedNode>) (isNodeToRemove: CfgNodeContent -> bool) (node: CfgRefinedNode) =
    let toBeRemoved (toRMnode: CfgNode) =
        (getUID node.node) <> (getUID toRMnode)
    let setChildrenParentage (children: CfgRefinedNode list) (newParents: CfgNode list) =
        let setChildParentage (child: CfgRefinedNode) =
            child.parents <- (List.filter toBeRemoved child.parents) @ newParents
        List.iter setChildParentage children
    let getParent node =
        if (parentages.ContainsKey(getUID node)) then 
            (parentages.[(getUID node)])
        else
            Log.ERROR <| sprintf "Node %s (Line: %d) has no parents" 
                                    (getUID node) 
                                    (lineOfNode node)
            { parents = []; node = node}
    let link() =
        let parents = node.parents
        let children = node.node.links
        List.iter (fun par -> Cfg.mutAppendLinks par children) parents
        // find parentage for each child and change it to new parentage
        setChildrenParentage (List.map (fun x -> getParent x) children) parents
    let removeNode() =
        //printfn "Removing node %s" (getUID node.node)
        //printfn "Before: %A" <| List.map (fun par -> List.map (fun y -> getUID y) par.links) node.parents 
        let parents = node.parents
        List.iter (fun par -> par.links <- List.filter toBeRemoved par.links) parents
        node.node.links <- [{ node = Pseudo("VALHALLA"); links = [] }] //removed node always points to VALHALLA
        //printfn "After: %A" <| List.map (fun par -> List.map (fun y -> getUID y) par.links) node.parents
    if (isNodeToRemove node.node.node) then
        link()
        removeNode()
   

let public refineCFG (root: CfgNode) =
    let parentage = restoreParentage root
    visited.Clear()
    let isExitNode = function
        | Exit(n) -> n.GetType() = typeof<BlockSyntax> 
                                    || n.GetType() = typeof<IfStatementSyntax>
                                    || n.GetType() = typeof<ElseClauseSyntax>
        | Entry(n) -> n.GetType() = typeof<BlockSyntax>
                                    || n.GetType() = typeof<ElseClauseSyntax>
        | _ -> false 

    Seq.iter (removeNodeWhen parentage isExitNode) parentage.Values
    {cfgn = root; tag = parentage} // modified root with tag - parentage
        