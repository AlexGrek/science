﻿module Utils

open Cfg

let getUID (node: CfgNode) =
    let syntax = node.node
    let text =
        match syntax with
        | Syntax s -> sprintf "\"%s\"" (Print.detailsToString s)
        | Pseudo p -> sprintf "\"%s\"" p
        | Entry n -> sprintf "\"ENTRY(%s)\"" (Print.detailsToString n)
        | Exit  n -> sprintf "\"EXIT(%s)\""  (Print.detailsToString n)
    "\"" + text.Replace("\"", "^") + " #" + (List.length node.links).ToString() + "\""

type Sth = Pirozhok of int | Tortik of string | Bulochka

let descr = function
    | [] -> 0
    | [ a ] -> 1 + a
    | [a; b] -> a + b
    | [a; b; c] -> a + b - c
    | head::tail ->  List.sum <| [head] @ tail