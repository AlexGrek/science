﻿module DotGraph

open System.Diagnostics

let mutable GRAPHVIZ = @"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe"

let public makeSvg name =
    printfn "Creating SVG file: %A" name
    Process.Start(GRAPHVIZ, sprintf "-Tsvg %s -o %s.svg" name (name.Replace(".dot", ""))).WaitForExit()
