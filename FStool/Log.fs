﻿module Log

open System
open System.IO

let mutable STAT_PATH = "./stat.txt"
let mutable LOG_PATH = "./log.txt"
let mutex = Object()

let setLogPath path = 
    if (File.Exists(path)) then
        File.Delete(path)
    LOG_PATH <- path

let setStatPath path = 
    if (File.Exists(path)) then
        File.Delete(path)
    STAT_PATH <- path


let printToFile path data =
    lock mutex
        (fun () -> File.AppendAllText(path, data + Environment.NewLine))


let printStatMessage (message: string) =
    printToFile STAT_PATH message


let printWithColor (color: ConsoleColor) (message: obj) =
    Console.ForegroundColor <- color
    Console.WriteLine(message)
    Console.ForegroundColor <- ConsoleColor.White


let WARNING (message: obj) =
    printWithColor ConsoleColor.Red message
    printToFile LOG_PATH <| "WARN: " + (message |> string)


let ERROR (message: obj) =
    printWithColor ConsoleColor.Red "!------- ERROR!!! -------!"
    printWithColor ConsoleColor.Red message
    printWithColor ConsoleColor.Red "!------------------------!"
    printToFile LOG_PATH <| "ERR!: " + (message |> string)


let INFO (message: obj) =
    printWithColor ConsoleColor.Yellow message
    printToFile LOG_PATH <| "INFO: " + (message |> string)


let SUCCESS (message: obj) =
    printWithColor ConsoleColor.Green message
    printToFile LOG_PATH <| "SUCC: " + (message |> string)

