﻿module Dot

open Cfg

let getUID (node: CfgNode) =
    let syntax = node.node
    let text =
        match syntax with
        | Syntax s -> sprintf "\"%s\"" (Print.justDetailsToString s)
        | Pseudo p -> sprintf "\"%s\"" p
        | Entry n -> sprintf "\"ENTRY(%s)\"" (Print.justDetailsToString n)
        | Exit  n -> sprintf "\"EXIT(%s)\""  (Print.justDetailsToString n)
    //"\"" + text.Replace("\"", "^") + " #" + (List.length node.links).ToString() + "\""
    text
    

let printHeader name =
    sprintf "digraph %s {\n" name

let printFooter = "}\n"

let visited = System.Collections.Generic.HashSet<string>()
    

let rec printDot (node: CfgNode) =
    if (visited.Contains(getUID node)) then
        [ "" ]
    else
        visited.Add(getUID node) |> ignore
        let makeLink link =
            sprintf "%s -> %s" (getUID node) (getUID link)
        (List.map makeLink node.links) @ List.concat (List.map printDot node.links)

    

let public printCfg name (root: CfgNode) =
    printfn "printing graph %s..." name
    visited.Clear()
    printHeader name + 
        (List.fold (fun r s -> r + s + "\n") "" <| printDot root) +
        printFooter