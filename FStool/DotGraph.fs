﻿module DotDump

open System

type GraphShape = Circle | Box | Trapezium | Parallelogram | DoubleCircle | Custom of string | Default

let shapeToString = function
    | Circle -> "circle"
    | Box -> "box"
    | Trapezium -> "trapezium"
    | Parallelogram -> "parallelogram"
    | DoubleCircle -> "doublecircle"
    | Custom s -> s
    | Default -> "default"

type DotGraphConfig<'a> = { getID: 'a -> string; 
                            getLinks: 'a -> string seq; 
                            getFullText: ('a -> string) option;
                            getShape: ('a -> GraphShape) option }

let generateGraph<'a> (getIdFun: 'a -> string) getLinksFun getFullTextFun getShapeFun =
    {getID = getIdFun; getFullText = getFullTextFun; getLinks = getLinksFun; getShape = getShapeFun}

let dotHeader name =
    sprintf "digraph %s {" name

let dotFooter = "}" + Environment.NewLine

let clearString (s: string) =
    s.Replace("\"", "")

let nodeToShape (graph: DotGraphConfig<'a>) item =
    let itemID = "\"" + (graph.getID item |> clearString) + "\""
    match graph.getShape with
    | None -> ""
    | Some(f) -> 
                let shape = f item
                match shape with
                | Default -> ""
                | _ -> sprintf "%s [shape=%s]" itemID (shapeToString shape |> clearString)

let nodeToFulltext (graph: DotGraphConfig<'a>) item =
    let itemID = "\"" + (graph.getID item |> clearString) + "\""
    match graph.getFullText with
    | None -> ""
    | Some(f) -> 
                let label = f item |> clearString
                match label with
                | "" -> ""
                | _ -> sprintf "%s [label=\"%s\"]" itemID label

let nodeToStringList<'a> (graph: DotGraphConfig<'a>) item =
    let printLink mother a =
        sprintf "\"%s\" -> \"%s\"" (graph.getID mother |> clearString) (clearString a)
    let links = graph.getLinks item
                |> Seq.map (printLink item)
                |> Seq.toList
    [(nodeToShape graph item); (nodeToFulltext graph item) ] @ links

let printGraphToStdout<'a> (graph: DotGraphConfig<'a>) (name: string) (data: 'a seq) =
    printfn "%s" (dotHeader name)
    Seq.map (nodeToStringList graph) data
    |> Seq.concat
    |> Seq.iter (printfn "%s")
    printfn "%s" dotFooter
    
let printGraphToString (graph: DotGraphConfig<'a>) (name: string) (data: 'a seq) =
    let data = 
        Seq.map (nodeToStringList graph) data
        |> Seq.concat
        |> Seq.reduce (fun acc el -> if (String.IsNullOrWhiteSpace(el)) 
                                     then acc 
                                     else acc + Environment.NewLine + el)
    (dotHeader name) + Environment.NewLine +
        data + Environment.NewLine +
        dotFooter

let printGraphToFile (graph: DotGraphConfig<'a>) path (name: string) (data: 'a seq) =
    let full = printGraphToString graph name data
    System.IO.File.WriteAllText(path, full)
