﻿module public FileIO

open System.Diagnostics

let public ensurePathExists path =
    if (not <| System.IO.Directory.Exists(path)) then
        System.IO.Directory.CreateDirectory(path) |> ignore

let mutable GRAPHVIZ = @"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe"

let mutable FILE_PATH = "."

let changeFilePath path =
    ensurePathExists path
    FILE_PATH <- path


let public writeDots (conf: DotDump.DotGraphConfig<'a>) (name, content: 'a list) =
    let file = System.IO.Path.Combine(FILE_PATH, name + ".dot")
    DotDump.printGraphToFile conf file name content


let public writeAndMakeSvg (conf: DotDump.DotGraphConfig<'a>) (name, content: 'a list) =
    let file = System.IO.Path.Combine(FILE_PATH, name + ".dot")
    DotDump.printGraphToFile conf file name content
    DotGraph.makeSvg file


let public makeAllSvgs conf =
    List.iter (writeAndMakeSvg conf)


let public makeAllDots conf =
    List.iter (writeDots conf)


let public makeAllSvgsParallel conf item =
    List.toArray item
    |> Array.Parallel.iter (writeAndMakeSvg conf)